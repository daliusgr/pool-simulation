package shapes;

public class Circle implements SimpleShape {

	public double x;	//x coordinate of the centre
	public double y;	//y coordinate of the centre
	public double r;	//radius of the circle
	
	public Circle(double x, double y, double r){
		this.x = x;
		this.y = y;
		this.r = r;
		
	}
	
	public Circle(){
		this(0,0,0);
	}
	
	//copy constructor
	public Circle(Circle crcl){
		this(crcl.x, crcl.y, crcl.r);
	}

	@Override
	public boolean intersects(SimpleShape shape) {
		if(shape instanceof Circle){
			Circle circle = (Circle) shape;
			double x = circle.x;
			double y = circle.y;
			double r = circle.r;

			double distance = Math.sqrt(Math.pow(this.x - x, 2)+Math.pow(this.y - y, 2));
			
			return (distance < this.r+r);
		}else if (shape instanceof Rectangle){
			//System.out.println("Shape rectangle.");
			Rectangle rect = (Rectangle) shape;
			
			//Does circle contain any of rectangle corners
			boolean corners = contains(rect.x, rect.y)||contains(rect.x, rect.y+rect.height)||
					contains(rect.x+rect.width, rect.y)||contains(rect.x, rect.y+rect.height);
			
			//Does rectangle contain any of upper, lower, left and right points of the circle
			boolean points = rect.contains(x, y-r)||rect.contains(x, y+r)||
					rect.contains(x-r, y)||rect.contains(x+r, y);
			
			return corners || points;
		}else{
			System.out.println("Shape not recognised.");
			return false;
		}
	}

	@Override
	//Returns true if bounds contain rectangle
	public boolean contains(Rectangle rect) {
		return bounds().contains(rect);
	}

	@Override
	//Returns true if circle contain point
	public boolean contains(double x, double y) {
		double distance = Math.sqrt(Math.pow(this.x - x, 2)+Math.pow(this.y - y, 2));
		
		return (distance <= r);
	}

	@Override
	public Rectangle bounds() {
		return new Rectangle(x-r, y-r, r+r, r+r);
	}
	
	@Override
	public String toString(){
		return "Circle("+x+", "+y+", "+r+");";
	}

	@Override
	public void move(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	

}
