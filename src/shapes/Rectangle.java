package shapes;


public class Rectangle implements SimpleShape{
	
	public double x;	//upper left corner x
	public double y;	//upper left corner y
	public double width;
	public double height;
	
	public Rectangle(double x, double y, double width, double height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public Rectangle(){
		this(0,0,0,0);
	}
	
	@Override
	public boolean intersects(SimpleShape shape) {
		if(shape instanceof Rectangle){
			Rectangle rect = (Rectangle)shape;
			if(contains(rect)){
				return true;
			}else{
				double x1 = rect.x;
				double y1 = rect.y;
				double x2 = x1+rect.width;
				double y2 = y1+rect.height;
				
				return contains(x1, y1) || contains(x1, y2) ||
						contains(x2, y1) || contains(x2, y2);
			}
		}else return shape.intersects(this);
		
	}
	
	@Override
	public boolean contains(double x, double y) {
		return (this.x <= x && this.x+width >= x
				&& this.y <= y && this.y+height >= y);
	}
	
	@Override
	public boolean contains(Rectangle rect) {
		double x1 = rect.x;
		double y1 = rect.y;
		double x2 = x1+rect.width;
		double y2 = y1+rect.height;

		return (contains(x1, y1) && contains(x2, y2));
	}
	@Override
	public Rectangle bounds() {
		return this;
	}
	
	@Override
	public String toString(){
		return "Rectangle("+x+", "+y+", "+width+", "+height+");";
	}

	@Override
	public void move(double x, double y) {
		this.x = x;
		this.y = y;
	}

}
