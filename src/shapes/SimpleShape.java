package shapes;

/*
 * Interface that provides a basic framework of shape
 * any shape can be derived from this class in 2D space
 * 
 * */
public interface SimpleShape {
	
	/*
	 * @return true if shape intersects with another shape*/
	public boolean intersects(SimpleShape shape);
	
	/*Check if given rectangle is in the bounds of the shape*/
	public boolean contains(Rectangle rect);
	
	/*Check if given rectangle is in the bounds of the shape*/
	public boolean contains(double x, double y);
	
	/*Return rectangular bounds of he object*/
	public Rectangle bounds();
	
	//Move shape to specified position
	public void move(double x, double y);
	
	/*Return string format of the object*/
	public String toString();
}
