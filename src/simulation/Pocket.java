package simulation;
import java.awt.Color;
import java.awt.Graphics;

import shapes.Circle;
import shapes.SimpleShape;

/*
 * Class representing a pocket in a pool table
 * */
public class Pocket extends Circle implements Collidable, Drawable{

	public Color color;
	public Circle collidable_shape;
	
	/*@param: center position of the pocket, radius, color*/
	Pocket(double x, double y, double radius, Color color) {
		super(x, y, radius);
		this.color = color;
		
		collidable_shape = new Circle(x, y, radius);
	}

	@Override
	public void render(Graphics g, Vector2D scale) {
		g.setColor(color);
		int w = (int)(this.r*2 * scale.x);
		int h = (int)(this.r*2 * scale.y);
		int x = (int)((this.x-r) * scale.x);
		int y = (int)((this.y-r) * scale.y);
		g.fillOval(x, y, w, h);
	}

	@Override
	public void onCollision(Collidable obj) {
	}

	@Override
	public SimpleShape getCollidable() {
		return collidable_shape;
	}
}
