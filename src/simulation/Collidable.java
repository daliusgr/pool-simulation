package simulation;

import shapes.SimpleShape;

/*interface for collidable object*/
interface Collidable {
	
	//Define action on collision
	public void onCollision(Collidable obj);
	
	public SimpleShape getCollidable();
}
