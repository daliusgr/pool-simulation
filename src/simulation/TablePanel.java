package simulation;
/*
	Panel for drawing simulation
*/

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JPanel;

class TablePanel extends JPanel{
	private int DELAY = 16;			//FPS
	private long timer = 0;			//timer for FPS rate
	private boolean start_sim = false;	//starts the simulation
	private boolean panel_stop = false;	//stops ball updates
	
	private String filename;		//input filename
	
	private Table table;
	private double table_aspect;	//aspect ration between width and height of the table
	
	private JLabel fps_label;
	
	private MouseControl mouseControl;
	
	private Vector2D scale;

	TablePanel(String filename){
		this.filename = filename;
		
		fps_label = new JLabel("FPS: ");
		//add(fps_label);
		
		DataHandler data = new DataHandler(filename);
		table = new Table(data.table_width, data.table_height, data.balls, 
				data.pockets, data.walls, Color.green);
		table_aspect = table.width/table.height;
		
		mouseControl = new MouseControl(data.balls);
		addMouseListener(mouseControl);
		
		timer = System.currentTimeMillis();
		
		scale = new Vector2D();
	}
	
	//@Override
	public void paintComponent(Graphics g){
		long time_diff = System.currentTimeMillis() - timer;
		
		scale.x = getSize().width/table.width;					//set x scale
		scale.y = (getSize().width/table_aspect)/table.height;	//set y scale by table proportions
		mouseControl.updateScale(scale);
		
		if(time_diff >= DELAY){
			super.paintComponent(g);
			fps_label.setText("FPS: "+1000/time_diff);
			
			table.render(g, scale);
			
			timer = System.currentTimeMillis();
		}

		double timestep = (double)time_diff/1000.0;
		table.update(timestep);
		
		repaint();
	}
}

