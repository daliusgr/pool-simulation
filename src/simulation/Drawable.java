package simulation;

import java.awt.Graphics;

public interface Drawable {
	//Every drawable supposed to have a render function that
	//draws itself to provided graphics, 
	//Since size data is stored in meters, scale is used for representing units in pixels
	//Scale is a 2d vector because x and y scale values will differ if frame is not square
	public void render(Graphics g, Vector2D scale);
}
