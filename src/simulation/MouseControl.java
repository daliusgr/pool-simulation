package simulation;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class MouseControl extends MouseAdapter{
	
	private double slow_down = 0.5;
	private double max_speed = 10;
	private Vector2D scale;
	
	boolean selected = false;	//true if a ball is selected
	Ball current;				//currently selected ball
	Ball last;					//last ball that was selected
	ArrayList<Ball> balls;		//balls that are mouse controlable
	
	BallTest set;
	
	MouseControl(ArrayList<Ball> balls){
		this.balls = balls;
		current = new Ball();
		last = new Ball();
		
		scale = new Vector2D();
		//set = new BallTest();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		double mousex = arg0.getX()/scale.x;
		double mousey = arg0.getY()/scale.y;
		if(arg0.getButton() == MouseEvent.BUTTON1){
			boolean selected_now = false;	//true if ball selected at this event
			//check if a ball is being selected
			for(int i = 0; i < balls.size(); i++)
				if(balls.get(i).contains(mousex, mousey)){
					current = balls.get(i);
					last = current;
					selected_now = true;
					selected = true;
				}
			
			//If a ball is selected but not now, that means it is being fired
			//launch the ball according to distance of the pointer from ball
			if(selected && !selected_now){
				Vector2D mouse = new Vector2D(mousex-current.x, mousey-current.y);
				
				current.vel = Vector2D.mul(slow_down, mouse);
				
				System.out.println(Math.toDegrees(mouse.getAngle()));
				System.out.println(mouse.getMagnitude());
				
			}
		}else{
			//deselect ball
			if(selected){
				selected = false;
				current = new Ball();
			}else{
					Ball ball;
						System.out.println(last.toString());
						ball = new Ball(last);
						ball.x = mousex;
						ball.y = mousey;
						balls.add(ball);
			}
		}
				
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void updateScale(Vector2D scale){
		this.scale = scale;
	}

}
