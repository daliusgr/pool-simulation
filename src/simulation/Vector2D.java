package simulation;
/*
	Class representing a 2D Vector
	Values by default are stored in Cartesian
*/

public class Vector2D{
	
	public double x;
	public double y;
		
	//Default constructor
	Vector2D(){
		this(0.0, 0.0);
	}
	
	//Explicit constructor
	public Vector2D(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	//Copy constructor
	public Vector2D(Vector2D vec){
		this(vec.x, vec.y);
	}
	
	//returns distance between this and other vector
	public double getDistance(Vector2D b){
		return Math.sqrt(Math.pow(this.x - b.x, 2) + 
			Math.pow(this.y - b.y, 2));	
	}
	
	/*Return magnitude of vector, zero if all components are zero*/
	public double getMagnitude(){
		double mag = Math.sqrt(x*x + y*y);
		if(Double.isNaN(mag)){
			return 0;
		}
		return mag;	
	}
	
	/* 
	 * @return angle of vector in radians
	 * */
	public double getAngle(){
		return Math.atan2(x, y);
	}
	
	/*@return this vector as a unit vector*/
	public Vector2D asUnit(){
		double scal;
		
		if(this.getMagnitude() == 0)
			scal = 0;
		else
			scal = 1/this.getMagnitude();
		
		return mul(scal, this);
	}
	
	public String toString(){
		return "("+x+"; "+y+")";
	}	
	
	public Vector2D copy(){
		return new Vector2D(x, y);
	}
	
	/*--------STATIC METHODS---------------*/
	
	/*@return distance between two vectors*/
	public static double distance(Vector2D v1, Vector2D v2){
		return Math.sqrt(Math.pow(v1.x-v2.x, 2)+Math.pow(v1.y-v2.y, 2));
	}
	
	/* (magnitude, angle) -> (x, y)
	 * @param Vector2D in polar coordinates
	 * @return Vector2D in Cartesian coordinates*/
	public static Vector2D toCartesian(double mag, double ang){

		double x = mag*Math.cos(ang);
		double y = mag*Math.sin(ang);
		
		return new Vector2D(x, y);
	}
	
	/*@param two Vector2D to add
	 * @return new Vector2D, result of addition
	 * */
	public static Vector2D add(Vector2D v1, Vector2D v2){
		return new Vector2D(v1.x+v2.x, v1.y + v2.y);
	}
	
	/*@param two Vector2D to substract
	 * @return new Vector2D, result of substraction
	 * */
	public static Vector2D sub(Vector2D v1, Vector2D v2){
		return new Vector2D(v1.x-v2.x, v1.y-v2.y);
	}
	
	/*@return product of scalar 'a' and a Vector2D
	 * */
	public static Vector2D mul(double a, Vector2D vec){
		return new Vector2D(vec.x*a, vec.y*a);
	}
	
	/*@return Vector2D which is projection of 'vec1' to 'vec2'
	 * */
	public static Vector2D project(Vector2D vec, Vector2D tovec){
		//angle between the vectors
		double ang = vec.getAngle() - tovec.getAngle();
		
		//proj = a*tovec.unit
		//a = (|vec|*cos(ang))	(scalar projection)
		
		return mul(vec.getMagnitude()*Math.cos(ang), tovec.asUnit());
	}
	
}
