package simulation;
import java.util.ArrayList;

/*Class representing collision handler.
 * Checks for collision among collidable objects and performs tasks according to
 * the types of objects that have collided
 * */
public class CollisionHandler {
	
	public static void processCollisions(ArrayList<Collidable> objects, ArrayList<Collidable> buffer){
		int size = objects.size();
		for(int i = 0; i < size-1; i++){
			Collidable obj1 = objects.get(i);
			Collidable obj1_buf = buffer.get(i);
			for(int j = i+1; j < size; j++){
				Collidable obj2 = objects.get(j);
				Collidable obj2_buf = buffer.get(j);
				if(obj1_buf.getCollidable().intersects(obj2_buf.getCollidable())){
					obj1.onCollision(obj2);
				}
			}
		}
	}

}
/*
 * Problem - balls get stuck together because collision handler is only checking for intersection,
 * if intersection is happening, next step of the ball usually gets it out of another,
 * but sometimes it's too deep. Some kind of pre-check is required.
 * 
 * Buffer is used for pre-checking, buffer provides object copies in next step
 * */
 