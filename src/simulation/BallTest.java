package simulation;
/*
	frame for testing stuff

*/

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

class BallTest extends JFrame{
	
	public Ball ball;

	BallTest(){
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(400, 300));
		//setLocation(new Point(300, 0));
		
		TestPanel test_pan = new TestPanel();
		add(test_pan);
		
		pack();
		setVisible(true);	
	}
	
	class TestPanel extends JPanel{
		
		JButton set_b;
		
		JTextField mass_f;
		JTextField rad_f;
		JTextField fric_f;
		JTextField r_f;
		JTextField g_f;
		JTextField b_f;
		
		JLabel mass_l;
		JLabel rad_l;
		JLabel fric_l;
		JLabel col_l;
		
		TestPanel(){
			setLayout(new GridLayout(5, 2));
			Container cont = new Container();
			cont.setLayout(new GridLayout(1, 3));
			
			mass_f = new JTextField("2", 12);
			rad_f = new JTextField("10", 12);
			fric_f = new JTextField("0.01", 12);
			
			r_f = new JTextField("255",3);
			g_f = new JTextField("255",3);
			b_f = new JTextField("255",3);
			cont.add(r_f); cont.add(g_f); cont.add(b_f);
			
			mass_l = new JLabel("mass");
			rad_l = new JLabel("radius");
			fric_l = new JLabel("friction");
			col_l = new JLabel("Color");
			set_b = new JButton("Set ball");
			
			add(mass_l); add(mass_f);
			add(rad_l); add(rad_f);
			add(fric_l); add(fric_f);
			add(col_l); add(cont);
			
			set_b.addActionListener(new ActionListener() {
				 
	            public void actionPerformed(ActionEvent e)
	            {
	                double mass = Double.valueOf(mass_f.getText());
	                double rad = Double.valueOf(rad_f.getText());
	                double fric = Double.valueOf(fric_f.getText());
	                int r = Integer.valueOf(r_f.getText());
	                int g = Integer.valueOf(g_f.getText());
	                int b = Integer.valueOf(b_f.getText());
	                
	                ball = new Ball(0, 0, new Vector2D(), mass, rad, fric, new Color(r, g, b));
	            }
	        });      
	 
			
			add(set_b);
			
			ball = new Ball(0, 0, new Vector2D(), 2, 10, 0.01, new Color(255, 255, 255));
		}
		
	}

}
