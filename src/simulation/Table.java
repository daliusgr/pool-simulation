package simulation;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import shapes.Rectangle;

/*
 * Class representing pool table.
 * Contains table dimension, pockets and balls
 * */
public class Table extends Rectangle implements Drawable{
	private Color color;
	private ArrayList<Ball> balls;
	private ArrayList<Pocket> pockets;
	private ArrayList<Wall> walls;
	
	Table(double width, double height, ArrayList<Ball> balls, ArrayList<Pocket> pockets, ArrayList<Wall> walls, Color color){
		super(0, 0, width, height);
		this.balls = balls;
		this.pockets = pockets;
		this.walls = walls;
		this.color = color;
	}
	
	//Render components to buffer
	public void render(Graphics g, Vector2D scale){
		//draw table
		g.setColor(color);
		g.fillRect(0, 0, (int)(width*scale.x), (int)(height*scale.y));
		
		//draw walls
		for(int i = 0; i < walls.size(); i++)
			walls.get(i).render(g, scale);
		
		//draw pockets
		for(int i = 0; i < pockets.size(); i++)
			pockets.get(i).render(g, scale);
		
		//draw balls
		for(int i = 0; i < balls.size(); i++)
			balls.get(i).render(g, scale);
	}
	
	public void update(double timestep){		
		ArrayList<Collidable> objects = new ArrayList<Collidable>(0);
		
		objects.addAll(balls);
		objects.addAll(pockets);
		objects.addAll(walls);
		
		/*
		 * Create a copy of balls array and process collision if they collide at next step
		 * */
		ArrayList<Collidable> buffer = new ArrayList<Collidable>(0);
		ArrayList<Ball> balls_next = new ArrayList<Ball>(0);
		for(Ball b: balls){
			Ball new_b;
				new_b = new Ball(b);
				new_b.update(timestep);
				balls_next.add(new_b);	
		}

		buffer.addAll(balls_next);
		buffer.addAll(pockets);
		buffer.addAll(walls);
		
		
		CollisionHandler.processCollisions(objects, buffer);
		
		for(int i = 0; i < balls.size(); i++)
			balls.get(i).update(timestep);
	}

}
