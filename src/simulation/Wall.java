package simulation;
import java.awt.Color;
import java.awt.Graphics;

import shapes.Rectangle;
import shapes.SimpleShape;


public class Wall extends Rectangle implements Collidable, Drawable{
	public Color color;
	private Rectangle collidable_shape;

	Wall(double x, double y, double w, double h, Color color) {
		super(x, y, w, h);
		this.color = color;
		collidable_shape = new Rectangle(x, y, w, h);
	}

	@Override
	public void render(Graphics g, Vector2D scale) {
		g.setColor(color);
		int w = (int)(this.width * scale.x);
		int h = (int)(this.height * scale.y);
		int x = (int)(this.x * scale.x);
		int y = (int)(this.y * scale.y);
		g.fillRect(x, y, w, h);
	}

	@Override
	public void onCollision(Collidable obj) {
	}

	@Override
	public SimpleShape getCollidable() {
		return collidable_shape;
	}

}
