package simulation;
/*
	Cannon ball simulation main class
*/

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JFrame;

class PoolSimulation{
	private static int GRAPH_WIDTH = 800;
	private static int GRAPH_HEIGHT = 600;

	public static void main(String args[]){	
		//TestFrame tf = new TestFrame();
		//BallTest tf = new BallTest();
		
		JFrame sim = new JFrame("Simulation");
		sim.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		sim.setSize(GRAPH_WIDTH, GRAPH_HEIGHT);
		sim.setPreferredSize(new Dimension(GRAPH_WIDTH, GRAPH_HEIGHT));
		sim.setLocation(new Point(0, 0));
		sim.setLayout(new BorderLayout());
		
		TablePanel sim_pan = new TablePanel("src/perfect_break.txt");
		sim.add(sim_pan);
		
		sim.pack();
		sim.setVisible(true);
	}
	
}
