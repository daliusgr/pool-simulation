package simulation;
/*
	Class for a ball
	Contains info:
	position, velocity, drag coeficient, radius, mass and color of the ball.
	Contains methods:
	Constructors, update and explicit set/get methods.
*/

import java.awt.Color;
import java.awt.Graphics;

import shapes.Circle;
import shapes.SimpleShape;

class Ball extends Circle implements Collidable, Drawable{
	
	private final double g = 9.8;		//default gravity pull
	
	//Initial coordinates of the ball
	private double start_x;
	private double start_y;	
	
	public Circle collidable_shape;		//shape of the ball that is collidable
	public Vector2D acc;		//(x, y) acceleration due to friction
	public Vector2D vel;		//(x, y) velocity of the ball (m/s)
	public double friction;		//friction coeficient 
	public double mass;			//mass of the ball
	public Color color;			//color of the ball
	
	//Copy constructor
	public Ball (Ball ball){
		this(ball.x, ball.y, new Vector2D(ball.vel), ball.mass, ball.r, ball.friction, ball.color);
	}
	
	//Default constructor
	Ball(){
		this(0, 0, new Vector2D(), 0.0, 0.0, 0.0, Color.black);	
	}
	
	//Explicit constructor
	Ball(double x, double y, Vector2D vel, double mass, double radius, 
		double friction, Color color){
			super(x, y, radius);
			this.vel = vel;
			this.mass = mass;
			this.friction = friction;
			this.color = color;

			collidable_shape = new Circle(x, y, radius);
			
			acc = new Vector2D();
			
			start_x = x;
			start_y = y;
	}
	
	
	//Updates position and velocity of the ball by explicitly 
	//defined time-step in seconds
	public void update(double timestep){
		//calculate acceleration caused by friction
		
		//before getting drag coefficient, check if velocity is not 0
		//double coef = 0;
		//if(vel.getMagnitude() > 0)
		//	coef = -friction*g/vel.getMagnitude();
		
		acc = Vector2D.mul(-friction*g, vel.asUnit());
			
		//r_x = r_x + v_x*t + 0.5*a_x*t*t
		x += vel.x*timestep +
			0.5*acc.x*timestep*timestep;
			
		//r_y = r_y + v_y*t + 0.5*a_y*t*t
		y += vel.y*timestep +
			0.5*acc.y*timestep*timestep;
		
		//If acceleration due to friction is larger than velocity,
		//that means it was truncated due to limited fraction representation in 
		//computer memory
		//Friction acceleration should not change direction of velocity
		if(vel.getMagnitude() < Vector2D.mul(timestep,acc).getMagnitude())
			vel = new Vector2D();
				
		//v_x = v_x + a_x*t;
		vel.x += acc.x*timestep;
			
		//v_y = v_y + a_y*t;
		vel.y += acc.y*timestep;
		
		//x and y belong to the shape, so object moving == shape moving
		collidable_shape.move(x, y);
	}
	
	@Override
	public void render(Graphics g, Vector2D scale) {	
		g.setColor(color);
		int w = (int)(this.r*2 * scale.x);
		int h = (int)(this.r*2 * scale.y);
		int x = (int)((this.x-r) * scale.x);
		int y = (int)((this.y-r) * scale.y);
		g.fillOval(x, y, w, h);
	}
	
	//returns current position and velocity of the ball as a string
	//Ball: (position_x; position_y), (angle; magnitude), mass, radius, friction, (red; green; blue)
	public String toString(){
		
		String pos_str = "("+x+"; "+y+"),";
		String vel_str = vel.toString()+", ";
		String mass_str = mass+", ";
		String rad_str = r+", ";
		String frict_str = friction+", ";
		String color_str = "("+color.getRed()+"; "+color.getGreen()+"; "+color.getBlue()+" )";
		return "Ball: "+pos_str+vel_str+mass_str+rad_str+frict_str+color_str;
	}

	@Override
	public void onCollision(Collidable obj) {
		
		if(obj instanceof Ball){
			
			Ball ball = (Ball)obj;
			
			//Vector2D col_line = new Vector2D(Math.abs(this.x-ball.x), Math.abs(this.y-ball.y));
			Vector2D col_line = new Vector2D(this.x-ball.x, this.y-ball.y);
			
			/*
			 * Pre-collision check is used to prevent balls getting stuck,
			 * but sometimes it's not enough, in that case they need to be separated
			 * along the collision line
			 * */
			
			if(this.intersects(ball)){
				double distance = Math.sqrt(Math.pow(this.x - ball.x, 2)+Math.pow(this.y - ball.y, 2));
				double diff = this.r+ball.r-distance;
				Vector2D move = Vector2D.mul(diff, col_line);
				this.x += move.x/2;
				this.y += move.y/2;
				ball.x -= move.x/2;
				ball.y -= move.y/2;
			}
			
			//Get parallel and perpendicular vectors of this ball to collision line
			Vector2D v1_par = Vector2D.project(this.vel, col_line);
			Vector2D v1_per = Vector2D.sub(this.vel, v1_par);
			
			//Get parallel and perpendicular vectors of the other ball to collision line
			Vector2D v2_par = Vector2D.project(ball.vel, col_line);
			Vector2D v2_per = Vector2D.sub(ball.vel, v2_par);
			
			double m1 = this.mass;
			double m2 = ball.mass;
			
			Vector2D v1_par_res;	//resulting perpendicular vectors after collision
			Vector2D v2_par_res;
			
			//v1 = (2*m2*v2 - v1*(m2-m1))/(m1+m2)
			v1_par_res = Vector2D.mul(1/(m1+m2), Vector2D.sub(Vector2D.mul(2*m2, v2_par),Vector2D.mul(m2-m1, v1_par)));
			
			//v2 = (2*m1*v1 - v2*(m1-m2))/(m2+m1)
			v2_par_res = Vector2D.mul(1/(m1+m2), Vector2D.sub(Vector2D.mul(2*m1, v1_par),Vector2D.mul(m1-m2, v2_par)));

			this.vel = Vector2D.add(v1_per, v1_par_res);
			ball.vel = Vector2D.add(v2_per, v2_par_res);
			
		}else if(obj instanceof Pocket){
			
			//Check if middle of the ball is inside pocket,
			//then the ball falls inside pocket
			Pocket pocket = (Pocket)obj;
			if(pocket.contains(this.x, this.y)){
				if(this.color.equals(new Color(255, 255, 255))){
					x = start_x;
					y = start_y;
					vel = new Vector2D();
				}else{
					//dispose
					x = 0;
					y = 0;
					vel = new Vector2D();
				}
			}
			
		}else if(obj instanceof Wall){
			
			//determine if it's a vertical or a horizontal 
			//wall and reverse speed accordingly
			Wall wall = (Wall) obj;
			if(wall.width > wall.height){
				//horizontal wall
				this.vel.y *= -1;
				
			}else if(wall.width < wall.height){
				//vertical wall
				this.vel.x *= -1;
				
			}
			
		}
		
	}

	@Override
	public SimpleShape getCollidable() {
		return collidable_shape;
	}
}