package simulation;
/*
	frame for testing stuff

*/

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import shapes.Circle;
import shapes.Rectangle;

class TestFrame extends JFrame{

	TestFrame(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(400, 300));
		setLocation(new Point(300, 0));
		
		TestPanel test_pan = new TestPanel();
		add(test_pan);
		
		pack();
		setVisible(true);	
	}
	
	class TestPanel extends JPanel implements KeyListener{
		Circle circle;
		
		Circle rect;
		
		JLabel inter_l;
		
		Vector2D a;
		Vector2D b;
		
		
		TestPanel(){
			circle = new Circle(100, 100, 20);
			rect = new Circle(50, 50, 15);
			inter_l = new JLabel("Intersects: "+circle.intersects(rect));
			add(inter_l);
			
			a = new Vector2D(100, -100);
			b = new Vector2D(50, 80);
			
			addKeyListener(this);
			setFocusable(true);
			requestFocusInWindow();
		}
		
		public void addNotify() {
	        super.addNotify();
	        requestFocus();
	    }
		
		public void keyPressed(KeyEvent e) {
			int step = 4;
			if(e.getKeyCode() == KeyEvent.VK_W){
				a.y -= step;
			}
			if(e.getKeyCode() == KeyEvent.VK_S){
				a.y += step;
			}
			if(e.getKeyCode() == KeyEvent.VK_A){
				a.x -= step;
			}
			if(e.getKeyCode() == KeyEvent.VK_D){
				a.x += step;
			}
			
			if(e.getKeyCode() == KeyEvent.VK_UP){
				b.y -= step;
			}
			if(e.getKeyCode() == KeyEvent.VK_DOWN){
				b.y += step;
			}
			if(e.getKeyCode() == KeyEvent.VK_LEFT){
				b.x -= step;
			}
			if(e.getKeyCode() == KeyEvent.VK_RIGHT){
				b.x += step;
			}
			repaint();
		}
	    public void keyReleased(KeyEvent e) { }
	    public void keyTyped(KeyEvent e) {
	    }
	    
	    int x = 200;
	    int y = 200;
		
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			
			g.setColor(Color.red);
			g.drawLine(x, y, (int)a.x+x, (int)a.y+y);
			
			g.setColor(Color.green);
			g.drawLine(x, y, (int)b.x+x, (int)b.y+y);
			
			g.setColor(Color.blue);
			Vector2D proj = Vector2D.project(a, b);
			g.drawLine(x, y, (int)proj.x+x, (int)proj.y+y);
			
			g.setColor(Color.yellow);
			Vector2D c = Vector2D.sub(a, proj);
			g.drawLine(x, y, (int)c.x+x, (int)c.y+y);
			
			inter_l.setText("Angle: "+((a.getAngle())));
			
			/*
			int r1 = (int)rect.r;
			int x1 = (int)rect.x;
			int y1 = (int)rect.y;
			g.setColor(Color.black);
			g.fillOval(x1-r1, y1-r1, r1+r1, r1+r1);
			
			int r2 = (int)circle.r;
			int x2 = (int)circle.x;
			int y2 = (int)circle.y;
			g.setColor(Color.black);
			g.fillOval(x2-r2, y2-r2, r2+r2, r2+r2);
			inter_l.setText("Intersects: "+circle.intersects(rect));*/
			repaint();
		}
	}

}
