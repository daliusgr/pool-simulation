package simulation;
/*
	class for reading data for simulation
*/

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

class DataHandler{
	
	//Strings representing data in the file
	private String TABLE = "Table";
	private String BALL = "Ball";
	private String COMMENT = "//";
	private String SEP_PARAM = ",";	//sign used to separate distinct parameters
	private String SEP_DEF = ":";	//sign used to separate definition from parameters
	private String SEP_ARG = ";";	//sign used to separate distinct arguments for a parameter (arg0; arg1)
	
	public double table_width;			//table dimension
	public double table_height;
	
	public ArrayList<Ball> balls;		//array of pool balls
	public ArrayList<Pocket> pockets;
	public ArrayList<Wall> walls;
	
	
	
	DataHandler(String filename){
		balls = new ArrayList<Ball>(0);
		pockets = new ArrayList<Pocket>(0);
		walls = new ArrayList<Wall>(0);
		
		readData(filename);
	}
	
	//reads data from file
	public void readData(String filename){
		FileReader datafile;
		try {
			datafile = new FileReader(filename);
			BufferedReader in = new BufferedReader(datafile);
			String line;

			/*Read file*/
			while ((line = in.readLine()) != null) {
				if(line.trim().equals(COMMENT)){
					//Do nothing on comment
				}
				else if(line.split(SEP_DEF)[0].equals(TABLE)){
					readTable(line.split(SEP_DEF)[1].trim());
				}
				else if(line.split(SEP_DEF)[0].equals(BALL)){
					balls.add(getBall(line.split(SEP_DEF)[1].trim()));
				}
			}	
	        
			datafile.close();
			in.close();
			
			
			
		} catch (FileNotFoundException e) {
			System.out.println("File "+filename+"Not found.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException, can't read line.");
			e.printStackTrace();
		}
	}
	
	//save balls to a specified file
	//TODO: finish it!
	public static void saveBalls(ArrayList<Ball> ballz, String filename)
		throws IOException{
		FileWriter outputfile = new FileWriter(filename);
		BufferedWriter out = new BufferedWriter(outputfile);
        
		String s;
        	
		for(int i = 0; i < ballz.size(); i++){
			s = ballz.get(i).toString();
			out.write(s, 0, s.length());
			out.newLine();
		}
        
		out.close();
		outputfile.close();
	}

	/*
	 * @return ball according to parameters in string
	 * param layout: 
	 * (position_x; position_y), (angle; magnitude), mass, radius, friction, (red; green; blue)
	 * */
	public Ball getBall(String param){
		//split data by space
		String split[] = param.trim().split(SEP_PARAM);
		for(int i = 0; i < split.length; i++)
			split[i] = split[i].trim();
		
		//Position
		String pos_args[] = getArgs(split[0]);
		double x		= Double.valueOf(pos_args[0]);
		double y		= Double.valueOf(pos_args[1]);
		
		//Velocity
		String vel_args[] = getArgs(split[1]);
		double angle	= Double.valueOf(vel_args[0])*Math.PI/180; 	//convert to radians
		double velocity	= Double.valueOf(vel_args[1]);
		Vector2D vel = Vector2D.toCartesian(velocity, angle);
		vel.y = -vel.y;	//invert speed
		
		double mass		= Double.valueOf(split[2]);
		double radius	= Double.valueOf(split[3]);
		double friction	= Double.valueOf(split[4]);
		
		//Color
		String col_args[] = getArgs(split[5]);
		int red = Integer.valueOf(col_args[0]);
		int green = Integer.valueOf(col_args[1]);
		int blue = Integer.valueOf(col_args[2]);
		Color color = new Color(red, green, blue);
		
		/*If error in split[] out of index occurs, 
		 *probably not all parameters are provided for the ball
		 **/
		
		//return the ball
		Ball ball = new Ball(x, y, vel, mass, radius, friction, color);
		
		//System.out.println(ball.toString());
		
		return ball;
	}
	
	/*
	 * @return table according to parameters in string
	 * param layout:
	 * (width; height), pocket_count, pocket_radius
	 *
	 * */
	public void readTable(String param){
		String split[] = param.split(SEP_PARAM);
		
		//get table dimension
		String args[] = getArgs(split[0]);
		
		/*register table dimension*/
		table_width 			= Double.valueOf(args[0]);
		table_height 			= Double.valueOf(args[1]);
		int pocket_count 		= Integer.valueOf(split[1].trim());
		double pocket_r			= Double.valueOf(split[2].trim());
		
		
		//Initialize pockets
				pockets.add(new Pocket(pocket_r, pocket_r, pocket_r, Color.black));
				pockets.add(new Pocket(table_width-pocket_r, pocket_r, pocket_r, Color.black));
				pockets.add(new Pocket(pocket_r, table_height-pocket_r, pocket_r, Color.black));
				pockets.add(new Pocket(table_width-pocket_r, table_height-pocket_r, pocket_r, Color.black));
				if(pocket_count == 6){
					pockets.add(new Pocket(table_width/2, pocket_r, pocket_r, Color.black));
					pockets.add(new Pocket(table_width/2, table_height-pocket_r, pocket_r, Color.black));
				}
		
		//Create walls according to table dimension and pocket radius
		double pocket_d = pocket_r*2;
		walls.add(new Wall(table_width-pocket_r, pocket_d, pocket_r, (table_height-pocket_d*2), Color.blue));
		walls.add(new Wall(0, pocket_d, pocket_r, (table_height-pocket_d*2), Color.blue));
		if(pocket_count == 4){
			walls.add(new Wall(pocket_d, 0, (table_width-pocket_d*2), pocket_r, Color.blue));
			walls.add(new Wall(pocket_d, table_height-pocket_r, (table_width-pocket_d*2), pocket_r, Color.blue));
		}else{
			double table_half= table_width/2;
			walls.add(new Wall(pocket_d, 0, table_half-pocket_r*3, pocket_r, Color.blue));
			walls.add(new Wall(table_half+pocket_r, 0, table_half-pocket_r*3, pocket_r, Color.blue));
			walls.add(new Wall(pocket_d, table_height-pocket_r, table_half-pocket_r*3, pocket_r, Color.blue));
			walls.add(new Wall(table_half+pocket_r, table_height-pocket_r, table_half-pocket_r*3, pocket_r, Color.blue));
		}
		
		
	}
	
	/*Returns array of Strings that contain "(e1; e2; e3..)" separated to
	 * ["e1","e2","e3",..]*/
	public String[] getArgs(String param){
		String temp[] = param.split(SEP_ARG);
		int last = temp.length-1;
		temp[0] = temp[0].substring(1);
		temp[last] = temp[last].substring(1, temp[last].length()-1);
		
		for(int i = 0; i < temp.length; i++)
			temp[i] = temp[i].trim();
		
		return temp;
	}
}

