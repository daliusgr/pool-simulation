package simulation;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;

/*
 * For calculating coordinates of 15 balls of 8-ball pool, excluding cue ball.
 * */

public class PerfectBreak {
	static double mass = 0.163;
	static double radius = 0.028575;
	static double friction = 0.005;
	
	public static void main(String[] args){
		get8Ball(new Vector2D(2.0066, 0.68074));
		
		//getCueBallAngle(new Vector2D(2.0066, 0.68074), new Vector2D(0.7366, 0.4));
	}
	
	//Write 15 ball triangle arrangement to file according to foot spot location
	public static void get8Ball(Vector2D foot_spot){
		ArrayList<Ball> balls = new ArrayList<Ball>(0);
		Color striped = new Color(255, 0, 0);
		Color solid = new Color(255, 255, 0);
		//x offset of the balls
		double x_off = Math.sqrt(Math.pow(radius*2, 2) - Math.pow(radius, 2));
		//First ball is in foot_spot, number 4, yellow
		balls.add(new Ball(foot_spot.x, foot_spot.y, new Vector2D(), mass, radius, friction, solid));
		System.out.println(balls.get(0).toString());
		//ball 15
		balls.add(new Ball(foot_spot.x+x_off, foot_spot.y-radius, new Vector2D(), mass, radius, friction, striped));
		//ball 2
		balls.add(new Ball(foot_spot.x+x_off, foot_spot.y+radius, new Vector2D(), mass, radius, friction, solid));
		//ball 6
		balls.add(new Ball(foot_spot.x+x_off*2, foot_spot.y-radius*2, new Vector2D(), mass, radius, friction, solid));
		//ball 8
		balls.add(new Ball(foot_spot.x+x_off*2, foot_spot.y, new Vector2D(), mass, radius, friction, solid));
		//ball 12
		balls.add(new Ball(foot_spot.x+x_off*2, foot_spot.y+radius*2, new Vector2D(), mass, radius, friction, striped));
		//ball 11
		balls.add(new Ball(foot_spot.x+x_off*3, foot_spot.y-radius*3, new Vector2D(), mass, radius, friction, striped));
		//ball 4
		balls.add(new Ball(foot_spot.x+x_off*3, foot_spot.y-radius, new Vector2D(), mass, radius, friction, solid));
		//ball 14
		balls.add(new Ball(foot_spot.x+x_off*3, foot_spot.y+radius, new Vector2D(), mass, radius, friction, striped));
		//ball 7
		balls.add(new Ball(foot_spot.x+x_off*3, foot_spot.y+radius*3, new Vector2D(), mass, radius, friction, solid));
		//ball 5
		balls.add(new Ball(foot_spot.x+x_off*4, foot_spot.y-radius*4, new Vector2D(), mass, radius, friction, solid));
		//ball 10
		balls.add(new Ball(foot_spot.x+x_off*4, foot_spot.y-radius*2, new Vector2D(), mass, radius, friction, striped));
		//ball 9
		balls.add(new Ball(foot_spot.x+x_off*4, foot_spot.y, new Vector2D(), mass, radius, friction, striped));
		//ball 3
		balls.add(new Ball(foot_spot.x+x_off*4, foot_spot.y+radius*2, new Vector2D(), mass, radius, friction, solid));
		//ball 13
		balls.add(new Ball(foot_spot.x+x_off*4, foot_spot.y+radius*4, new Vector2D(), mass, radius, friction, striped));
		
		try {
			DataHandler.saveBalls(balls, "src/8balls_triangle.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Returns angle between ball and foot  spot in degree
	public static double getCueBallAngle(Vector2D foot_spot, Vector2D ball_pos){
		double c = foot_spot.getDistance(ball_pos);
		double b = foot_spot.getDistance(new Vector2D(ball_pos.x, foot_spot.y));
		double angle = Math.toDegrees(Math.asin(b/c));
		
		if(ball_pos.y < foot_spot.y)
			angle += 270;
		
		return angle;
	}
	
}
